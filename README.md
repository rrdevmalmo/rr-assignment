# Assignment instructions

Fork this repo into a *private* repository of yours.
When you are done with your assignment, add rrdevmalmo to your private fork with Read access, and respond to this email to let
us know you are done.

Do not create a pull request to my repo, because if you do, other people may see and review your changes. Pull requests are not
 private...

## Required

Fixing the Bug in Buggy

Rewrite the method in Specific to use anonymous inner classes implementing the three functional interfaces in the
com.rr.assignment.function package.

## Nice-to-haves

A build file for a build tool such as maven, ant, ivy, gradle, ...

A Test case that fails on the bug in Buggy, but passes after your fix.

Write a SpecificTest class in src/test/java that implement test cases for the Specific reachOut() method.