package com.rr.assignment;

/**
 */
public class Buggy {

  public static final String[] DAYS_OF_WEEK = {
    "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"
  };

  public static String nextDayOfWeek(String today) {
    for (int i = 0; i < DAYS_OF_WEEK.length; i++) {
      if (today.equalsIgnoreCase(DAYS_OF_WEEK[i])) {
        return DAYS_OF_WEEK[i+1];
      }
    }
    throw new IllegalArgumentException("No such day: " + today);
  }

}
